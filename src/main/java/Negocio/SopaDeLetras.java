package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class SopaDeLetras {

    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public void asignarCantFilas(int filas) {
        this.sopas = new char[filas][];
    }

    public void asignarCantColumnas(int fila, int columnas) {
        this.sopas[fila] = new char[columnas];
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2(char[] arreglo) {
        String msg = "";
        for (int i = 0; i < arreglo.length; i++) {
            msg += arreglo[i] + "\t";
        }
        return msg;
    }

    public boolean esCuadrada() {
        boolean esCuadrada = true;
        for (int i = 0; i < sopas.length && esCuadrada == true; i++) {
            esCuadrada = sopas.length == sopas[i].length;
        }
        return esCuadrada;
    }

    public boolean esDispersa() {
        boolean esDispersa = false;
        int cantidadColumnasFila0 = this.sopas[0].length;

        for (int i = 1; i < sopas.length && esDispersa == false; i++) {
            esDispersa = cantidadColumnasFila0 != this.sopas[i].length;
        }
        return esDispersa;

    }

    public boolean esRectangular() {
        boolean esRectangular = false;
        esRectangular = esCuadrada() == false && esDispersa() == false;
        return esRectangular;
    }

    /*retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) {
        int contador = 0;
        int k = 0;
        String palabrasSinEspacios = palabra.replace(" ", "");
        char buscar[] = new char[palabrasSinEspacios.length()];

        for (int i = 0; i < palabrasSinEspacios.length(); i++) {
            buscar[i] = palabrasSinEspacios.charAt(i);
        }

        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (sopas[i][j] == buscar[k]) {
                    k++;
                } else {
                    k = 0;
                    if (sopas[i][j] == buscar[k]) {
                        k++;
                    }
                }
                if (k == buscar.length) {
                    contador++;
                    k = 0;
                }
            }
            k = 0;
        }
        return contador;

    }

    /*debe ser cuadrada sopas
     */
    public char[] getDiagonalPrincipal() throws Exception {
        if (esCuadrada() == false) {
            throw new Exception("la matriz no es cuadra. por lo tanto, no tiene diagonal principal");
        }
        //comenzar a sacar la diagonal         
        char diagonal[] = new char[sopas.length];

        for (int i = 0; i < diagonal.length; i++) {
            diagonal[i] = sopas[i][i];
        }

        return diagonal;

    }

    public void buscarPalabra(String palabra) {
        char[] letras = palabra.toCharArray();

        buscarALaDerecha(letras);
        buscarALaIzquierda(letras);
        buscarArribaAbajo(letras);
        buscarAbajoArriba(letras);
        buscarDiagonales(letras);
    }

    private void buscarALaDerecha(char[] letras) {
        int contador = 0;
        int posicion = 0;
        int fila = 0;
        int columna = 0;

        for (int i = 0; i < sopas.length; i++) {
            if (contador == letras.length) {
                System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de izquierda a derecha");
            }
            contador = 0;
            posicion = 0;

            for (int j = 0; j < sopas[i].length; j++) {
                if (contador == letras.length) {
                    System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de izquierda a derecha");
                    contador = 0;
                    posicion = 0;
                }
                if (letras[posicion] == sopas[i][j]) {
                    contador++;
                    if (posicion == 0) {
                        fila = i;
                        columna = j;
                    }
                    posicion++;

                } else {
                    contador = 0;
                    posicion = 0;
                    if (letras[posicion] == sopas[i][j]) {
                        contador++;
                        if (posicion == 0) {
                            fila = i;
                            columna = j;
                        }
                        posicion++;
                    }
                }
            }
        }
        if (contador == letras.length) {
            System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de izquierda a derecha");
        }
    }

    private void buscarALaIzquierda(char[] letras) {
        int contador = 0;
        int posicion = 0;
        int fila = 0;
        int columna = 0;

        for (int i = 0; i < sopas.length; i++) {
            if (contador == letras.length) {
                System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de derecha a izquierda");
            }
            contador = 0;
            posicion = 0;

            for (int j = (sopas[i].length) - 1; j >= 0; j--) {
                if (contador == letras.length) {
                    System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de derecha a izquierda");
                    contador = 0;
                    posicion = 0;
                }
                if (letras[posicion] == sopas[i][j]) {
                    contador++;
                    if (posicion == 0) {
                        fila = i;
                        columna = j;
                    }
                    posicion++;

                } else {
                    contador = 0;
                    posicion = 0;
                    if (letras[posicion] == sopas[i][j]) {
                        contador++;
                        if (posicion == 0) {
                            fila = i;
                            columna = j;
                        }
                        posicion++;
                    }
                }
            }
        }
        if (contador == letras.length) {
            System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de derecha a izquierda");
        }
    }

    private void buscarArribaAbajo(char[] letras) {
        int contador = 0;
        int posicion = 0;
        int fila = 0;
        int columna = 0;

        for (int j = 0; j < sopas[0].length; j++) {
            if (contador == letras.length) {
                System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de arriba a abajo");
            }
            contador = 0;
            posicion = 0;

            for (int i = 0; i < sopas.length; i++) {
                if (contador == letras.length) {
                    System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de arriba a abajo");
                    contador = 0;
                    posicion = 0;
                }
                if (letras[posicion] == sopas[i][j]) {
                    contador++;
                    if (posicion == 0) {
                        fila = i;
                        columna = j;
                    }
                    posicion++;

                } else {
                    contador = 0;
                    posicion = 0;
                    if (letras[posicion] == sopas[i][j]) {
                        contador++;
                        if (posicion == 0) {
                            fila = i;
                            columna = j;
                        }
                        posicion++;
                    }
                }
            }
        }
        if (contador == letras.length) {
            System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de arriba a abajo");
        }
    }

    private void buscarAbajoArriba(char[] letras) {
        int contador = 0;
        int posicion = 0;
        int fila = 0;
        int columna = 0;

        for (int j = 0; j < sopas[0].length; j++) {
            if (contador == letras.length) {
                System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de abajo a arriba");
            }
            contador = 0;
            posicion = 0;

            for (int i = sopas.length - 1; i >= 0; i--) {
                if (contador == letras.length) {
                    System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de abajo a arriba");
                    contador = 0;
                    posicion = 0;
                }
                if (letras[posicion] == sopas[i][j]) {
                    contador++;
                    if (posicion == 0) {
                        fila = i;
                        columna = j;
                    }
                    posicion++;

                } else {
                    contador = 0;
                    posicion = 0;
                    if (letras[posicion] == sopas[i][j]) {
                        contador++;
                        if (posicion == 0) {
                            fila = i;
                            columna = j;
                        }
                        posicion++;
                    }
                }
            }
        }
        if (contador == letras.length) {
            System.out.println("se encontró en la fila " + fila + " con columna " + columna + " de abajo a arriba");
        }
    }

    private void buscarDiagonales(char[] letras) {
        int fila = 0;
        int columna = 0;
        for (int i = 0; i < sopas.length; i++) {
            for (int j = 0; j < sopas[i].length; j++) {
                if (sopas[i][j] == letras[0]) {
                    fila = i;
                    columna = j;
                    buscarDiagonalSuperiorIzquierda(letras, i, j,fila,columna);
                    buscarDiagonalSuperiorDerecha(letras, i, j,fila,columna);
                    
                    buscarDiagonalInferiorIzquierda(letras, i, j,fila,columna);
                    buscarDiagonalInferiorDerecha(letras, i, j,fila,columna);
                }
            }
        }
    }

    private void buscarDiagonalSuperiorIzquierda(char[] letras, int i, int j,int fila,int columna) {
        int contador = 1;
        int posicion = 1;
        i--;
        j++;

        while (contador <= letras.length) {  
            
            if (contador == letras.length) {
                System.out.println("se encontró una diagonal superior en la fila " + fila + " con columna " + columna + " de izquierda a derecha");
                break;
            }
            if(i >= 0 && i < sopas.length && j >= 0 && j < sopas[0].length ){
            if (letras[posicion] == sopas[i][j]) {
                contador++;
                posicion++;
            } else {
                break;
            }
            }else{
            break;
            }
            
            i--;
            j++;
        }       
    }

    private void buscarDiagonalSuperiorDerecha(char[] letras, int i, int j,int fila,int columna) {
        int contador = 1;
        int posicion = 1;
        i--;
        j--;

        while (contador <= letras.length) {  
            
            if (contador == letras.length) {
                System.out.println("se encontró una diagonal superior en la fila " + fila + " con columna " + columna + " de derecha a izquierda");
                break;
            }
            if(i >= 0 && i < sopas.length && j >= 0 && j < sopas[0].length ){
            if (letras[posicion] == sopas[i][j]) {
                contador++;
                posicion++;
            } else {
                break;
            }
            }else{
            break;
            }
            
            i--;
            j--;
        } 
    }

    private void buscarDiagonalInferiorIzquierda(char[] letras, int i, int j,int fila,int columna) {
        int contador = 1;
        int posicion = 1;
        i++;
        j--;

        while (contador <= letras.length) {  
            
            if (contador == letras.length) {
                System.out.println("se encontró una diagonal inferior en la fila " + fila + " con columna " + columna + " de derecha a izquierda");
                break;
            }
            if(i >= 0 && i < sopas.length && j >= 0 && j < sopas[0].length ){
            if (letras[posicion] == sopas[i][j]) {
                contador++;
                posicion++;
            } else {
                break;
            }
            }else{
            break;
            }
            
            i++;
            j--;
        } 
    }

    private void buscarDiagonalInferiorDerecha(char[] letras, int i, int j,int fila,int columna) {
        int contador = 1;
        int posicion = 1;
        i++;
        j++;

        while (contador <= letras.length) {  
            
            if (contador == letras.length) {
                System.out.println("se encontró una diagonal inferior en la fila " + fila + " con columna " + columna + " de izquierda a derecha");
                break;
            }
            if(i >= 0 && i < sopas.length && j >= 0 && j < sopas[0].length ){
            if (letras[posicion] == sopas[i][j]) {
                contador++;
                posicion++;
            } else {
                break;
            }
            }else{
            break;
            }
            
            i++;
            j++;
        } 
    }

    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    public void setValorMatriz(int fila, int columna, char valor) {
        this.sopas[fila][columna] = valor;
    }
}
